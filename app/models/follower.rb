class Follower < ActiveRecord::Base
	has_many :users
   validates :following_user, presence: true
   validates :followed_user, presence: true
end
