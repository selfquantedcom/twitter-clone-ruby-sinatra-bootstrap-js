require 'byebug'

get "/tweets" do

   if logged_in? == nil
      redirect '/login'
   end

   current_user
   @tweets = Tweet.order("updated_at DESC").all  
   erb :"static/tweets"

end

post '/tweets' do   
   current_user
   tweet = Tweet.new(:user_id => @current_user.id, :content => params[:user][:tweet])
   tweet.save      
   redirect "/tweets"
   
end

post "/delete_tweet/:id" do   

   #validate deletion rights and delete
   tweet = Tweet.find(params[:id])
   current_user

   if tweet.user_id == current_user.id 
      tweet.destroy   
      redirect "/tweets"
   end
end

post "/update_tweet/:id" do   
   
   #validate deletion rights and delete
   tweet = Tweet.find(params[:id])
   current_user

   if tweet.user_id == current_user.id 
      tweet.content = params[:user][:update_tweet]
      tweet.save   
      redirect "/tweets"
   end
end

