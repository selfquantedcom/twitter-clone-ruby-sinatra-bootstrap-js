require_relative 'login_controller'
require_relative 'users_controller'

require 'time_difference'
enable :sessions

get '/' do
 erb :"static/index"
end