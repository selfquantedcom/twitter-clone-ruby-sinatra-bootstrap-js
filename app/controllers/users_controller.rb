
post '/register' do
  user = User.new(params[:user])     
  user.save

  if user.save
    redirect '/login' 
 else
    redirect '/registration'
 end  

end

get "/static/registration" do    
 erb :"static/registration"
end


#Profile page
get "/static/profile/:id" do 

 if logged_in? == nil
  redirect '/login'
end
current_user
@user = User.find(params[:id])
erb :"static/profile"

end

post '/follow/:id' do  

   #has current_user already followed the other user?
   current_user
   following = Follower.where(:followed_user => params[:id], :following_user => current_user.id)
   
   #create new record
   if following[0] == nil

      new_following = Follower.new(:followed_user => params[:id], :following_user => current_user.id)
      new_following.save

   end

   redirect "/static/profile/#{params[:id]}"
end
