class CreateFollowers < ActiveRecord::Migration[5.0]
   def change
      # HINT: checkout ActiveRecord::Migration.create_table
      create_table :followers do |t|         
         t.integer :following_user
         t.integer :followed_user  

         t.timestamps
      end
   end
end
