require_relative '../app/models/user'
require_relative '../app/models/follower'
require_relative '../app/models/tweet'

user1 = User.new(:name => "1User", :email => "1email", :password => "pswd")
user2 = User.new(:name => "2User", :email => "2email", :password => "pswd")
user3 = User.new(:name => "3User", :email => "3email", :password => "pswd")
user4 = User.new(:name => "4User", :email => "4email", :password => "pswd")
user5 = User.new(:name => "5User", :email => "5email", :password => "pswd")
user6 = User.new(:name => "6User", :email => "6email", :password => "pswd")
user7 = User.new(:name => "7User", :email => "7email", :password => "pswd")
user8 = User.new(:name => "8User", :email => "8email", :password => "pswd")

user1.save
user2.save
user3.save
user4.save
user5.save
user6.save
user7.save
user8.save

tw1 = Tweet.new(:user_id => user1.id, :content => "1Tweet")
tw2 = Tweet.new(:user_id => user2.id, :content => "2Tweet")
tw3 = Tweet.new(:user_id => user3.id, :content => "3Tweet")
tw4 = Tweet.new(:user_id => user4.id, :content => "4Tweet")

tw1.save
tw2.save
tw3.save
tw4.save


